const btn = document.querySelector('.find')
const ul = document.createElement('ul')
const container = document.querySelector('.container')

async function getIP() {
    btn.addEventListener('click', async () => {
        try {
            const ipURL = await fetch('https://api.ipify.org/?format=json')
            const ipNum = await ipURL.json()

            const ipInfo = await fetch(`http://ip-api.com/json/${ipNum.ip}`)
            const info = await ipInfo.json()
            console.log(info);

        ul.innerHTML = `
        <li>country:${info.country}</li>
        <li>city:${info.city}</li>
        <li>region:${info.region}</li>
        <li>region name:${info.regionName}</li>
        `

        container.appendChild(ul)
        } catch (err) {
            throw new Error(err.message)
        }
    })

}

getIP()